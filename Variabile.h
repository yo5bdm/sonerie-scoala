/*  
    Aceasta este partea software a unei instalatii automate de sonerie pentru scoli
    Pentru mai multe detalii, cititi fisierul readme.md
    Adresa proiectului https://gitlab.com/yo5bdm/sonerie-scoala
    Copyright (C) 2018  Erdei Rudolf (https://erdeirudolf.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

byte butoane[] = {8,7,6,5};
//8 - back / cancel
//7 - UP
//6 - DOWN
//5 - OK/Enter
const byte nrButoane = 4;
int dimMeniu = 5;
int nrMeniu=0, nrSubmeniu=-1;
int stat, btn; //status
int debounce = 10;

String menu[] = {
  "Act/Dezac Sunat", //activeaza/dezactiveaza sunatul
  "Adauga Ora Noua",
  "Sterge ora",
  "Seteaza zile",
  "Set ora curenta",
};
int setOra[5] = {2018, 3, 13, 8, 0}; //an, luna, zi, ora, minut
String submeniu[] = {
  "Act/Dezac Sunat>",
  
  "Adauga> Ora",
  "Adauga> Minut",
  
  "Sterge> Alege",

  "Zile de sunat",
  
  "Set> Anul:",
  "Set> Luna:",
  "Set> Ziua:",
  "Set> Ora:",
  "Set> Minut:",
};
String zileText[]={
  "Duminica",
  "Luni",
  "Marti",
  "Miercuri",
  "Joi",
  "Vineri",
  "Sambata"
}; //textul zilelor de sunat

String months[] = { "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
//setari automat
bool enabled = true; //by default, automatul e oprit
int secundeSunat = 2;
int releu = 10; //pinul releului

//             D  L  M  M  J  V  S
byte ziua[7] = {0, 1, 1, 1, 1, 1, 0}; //zilele in care suna
int ziuaAcum;
int ziuaDeSetat=0;
// --------------- LCD
