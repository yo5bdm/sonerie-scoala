/*  
    Aceasta este partea software a unei instalatii automate de sonerie pentru scoli
    Pentru mai multe detalii, cititi fisierul readme.md
    Adresa proiectului https://gitlab.com/yo5bdm/sonerie-scoala
    Copyright (C) 2018  Erdei Rudolf (https://erdeirudolf.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Wire.h"
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>
#include "RTCLib.h"
#include "Ora.h"
#include "Variabile.h"
#include "print.h"
#include "Memorie.h"
#include "Meniu.h"

void setup() {
  pr.init(); //initializare display
  pinMode(releu, OUTPUT);
  digitalWrite(releu, HIGH); //high si low sunt inversate
  for(int i=0;i<nrButoane;i++) { //setam butoanele
    pinMode(butoane[i],INPUT_PULLUP);
  }
  if (! RTC.isrunning()) {
    pr.eroare("RTC Nu functioneaza");
    enabled = false;
    delay(2000);
  }
  oraNoua = new Ora(8,0);
  now = RTC.now();
  oraCurenta = new Ora(now.hour(), now.minute());
  stat=0;
  ziuaAcum = now.dayOfWeek(); //// Day of the week (1-7), Sunday is day 1
  if(Memorie::incarca()){
    pr.mesaj("Incarcat date");    
    delay(2000);
  } else {
    pr.eroare("Eroare incarcare");
    enabled = false;
    delay(2000);
  }
  Ora::rew();
  Serial.begin(9600);
  Serial.println("Setup gata");
}

void loop() {
  // verifica ziua
  if(!RTC.isrunning()) { //daca nu avem ceas, nu functioneaza nimic
    pr.eroare("Eroare RTC!!!"); 
    delay(2000); 
  } else {
    now = RTC.now(); //luam ora curenta
    ziuaAcum = now.dayOfWeek(); //ziua curenta
    oraCurenta->seteaza(now.hour(), now.minute()); //prelucram ora curenta
    if(stat == 0) { //meniu sau main loop?
      pr.mainScreen(); //printeaza main screen daca nu exista deja
      if(enabled && ziua[ziuaAcum]==1) { //activat daca ziua e activata si din setari
        if(Ora::compara(oraCurenta, Ora::cap) == 0) {
          //suna
          pr.mesaj("     Suna!");
          digitalWrite(releu, LOW);
          delay(secundeSunat * 1000);
          digitalWrite(releu, HIGH);
          Ora::inainte();
          pr.suna();
          pr.mainScreen();
        }
        pr.oraCurentaPrint();
        pr.oraSunat();
      } else {
        pr.oraCurentaPrint();
      }
      delay(500);
      stat = butonOK();
    } else {
      printMenu();
      btn = butonApasat();
      meniu(btn);
      delay(100);
    }
  }
}
