Despre aplicație

Aceasta este partea software a unei instalatii automate de sonerie pentru scoli.
Codul sursă este oferit sub licență GPL v3 (pentru detalii accesați fișierul 
GPLv3.txt)

Aplicația folosește o listă dublu-înlănțuită circulară pentru reținerea orelor la 
care automatul trebuie să sune.