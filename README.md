# Despre aplicație
Aceasta este partea software a unei instalatii automate de sonerie pentru scoli.
Codul sursă este oferit sub licență **GPL v3** (pentru detalii accesați fișierul 
GPLv3.txt). Se poate folosi liber pentru orice aplicatie ne-comerciala. Sunteti liber 
sa modificati codul sursa astfel incat acesta sa corespunda cerintelor Dvs, cu conditia
ca acesta sa nu fie comercializat si sa fie oferit mai departe tot sub licenta GPL v3.
Utilizarea comerciala este posibila doar cu achizitionarea unei licente. Pentru detalii 
va rog sa ma contactati la adresa **contact@erdeirudolf.com**

Aplicația folosește o listă dublu-înlănțuită circulară pentru reținerea orelor la 
care automatul trebuie să sune.

Schema o puteti deschide folosind aplicatia [fritzing](http://fritzing.org/).

Pentru manualul de utilizare, deschideti fisierul **Utilizare.doc**


# Piese folosite
1 x Arduino

1 x RTC DS1307

1 x Releu 

1 x LCD 16x2 cu shield I2C (pentru limitarea numarului de fire necesare)

4 x pushbutton (butoanele de interactiune cu meniul)


# Conexiunile
Pinii arduino - conexiunea

 5 - Buton **OK**

 6 - Buton **Down**

 7 - Buton **Up**

 8 - Buton **Cancel**
 
 10 - Comanda Releu
 
 A4, A5 - SCL, SDA - conexiune LCD

 SCL, SDA (langa butonul de reset) - conexiunea pentru RTC


# Probleme cunoscute [in lucru / continua schimbare]
1. Aplicatia, in aceasta versiune, **nu face setarea ceasului RTC**, asadar, inainte de-a utiliza aplicatia, trebuie facuta setarea ceasului. Odata setat ceasul, aplicatia porneste si daca nu este corecta ora, aceasta se poate corecta din meniu. (problema urmeaza sa fie corectata)
2. Atat la LCD cat si la RTC, trebuie aflata adresa I2C. Pentru aceasta va sugerez sa folositi [i2cScanner](https://playground.arduino.cc/Main/I2cScanner).