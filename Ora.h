/*  
    Aceasta este partea software a unei instalatii automate de sonerie pentru scoli
    Pentru mai multe detalii, cititi fisierul readme.md
    Adresa proiectului https://gitlab.com/yo5bdm/sonerie-scoala
    Copyright (C) 2018  Erdei Rudolf (https://erdeirudolf.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Ora;
Ora *oraCurenta; //cu asta lucreaza algoritmul
Ora *oraNoua; //pentru meniul adaugare ora
RTC_DS1307 RTC; //realtime clock
DateTime now;
class Ora {
  public:
    static Ora *cap;
    Ora *next, *prev;
    byte ora;
    byte minut;
    Ora(byte h, byte m) { //constructor
      this->prev = this;
      this->next = this;
      ora = h,
      minut = m;
    }
    void addNext(Ora *nx) { //seteaza pointerii next si prev
      this->next = nx;
      nx->prev = this;
    }
    void seteaza(byte h, byte m) { //modifica un obiect
      ora = h,
      minut = m;
    }
    static void insereaza(byte h, byte m) { //insereaza un element in lista in pozitia corecta
      Ora *nou = new Ora(h,m);
      if(Ora::cap == NULL) { //fara elemente
        Ora::cap = nou;
        return;
      }
      if(Ora::cap == Ora::cap->next) { //un singur element
        insereazaAici(Ora::cap,nou,Ora::cap->next);
        return;
      }
      rew();
      while(pozitie(Ora::cap,nou,Ora::cap->next)!=0) {
        if(compara(Ora::cap,Ora::cap->next)==1) { //suntem la capatul listei
            break;
        }
        Ora::inainte();
      }
      insereazaAici(Ora::cap,nou,Ora::cap->next);
      rew();
    }
    static void inainte() { //merge inainte in lista
      if(Ora::cap!=NULL) Ora::cap = Ora::cap->next;
    }
    static void inapoi() { //merge inapoi in lista
      if(Ora::cap!=NULL) Ora::cap = Ora::cap->prev;
    }
    static String afOra(Ora *af){
        if(af==NULL) return "Fara ora";
        String ora, minut;
        if(af->ora<10) ora = "0"+String(af->ora); else ora = String(af->ora);
        if(af->minut<10) minut = "0"+String(af->minut); else minut = String(af->minut);
        return ora+String(":")+minut;
    }
    static void rew() { //capul va fi urmatoarea ora de sunat
        now = RTC.now();
        if(cap == cap->next || cap==NULL) {
          return;
        }
        oraCurenta->seteaza(now.hour(),now.minute());
        if(compara(cap->prev,cap)==1) Ora::inapoi(); //bug cand ora curenta e exact inainte de prima din lista
        while(Ora::pozitie(cap->prev,oraCurenta,cap)!=0) {
            Ora::inapoi();
        }
    }
    static void insereazaAici(Ora *inainte, Ora *insereaza, Ora *dupa) {
        inainte->addNext(insereaza);
        insereaza->addNext(dupa);
    }
    static int compara(Ora *unu, Ora *doi) {
        if(unu==NULL || doi == NULL) return -2;
        if(unu->ora < doi->ora) return -1;
        if(unu->ora > doi->ora) return 1;
        if(unu->minut < doi->minut) return -1;
        if(unu->minut > doi->minut) return 1;
        return 0;
    }
    static int pozitie(Ora *before, Ora *asta, Ora *after) { // <- -1   0   1 ->
        int c1 = compara(before,asta);
        int c2 = compara(asta, after);
        int cn = compara(before,after);
        if(cn == 1) return 0;
        if(c1==-1 && c2==-1) return 0;
        if(c1==-1  && c2==1) return 1;
        if(c1==1 && c2==-1) return -1;
        if(c1==1 && c2==1) return -2;
        return 2;
    }
    static void sterge() { //sterge capul
        //daca capul e null
        if(cap==NULL) return;
        if(cap == cap->next) {
          Ora *sterge = Ora::cap;
          cap = NULL;
          delete sterge;
          return;
        }
        //daca e un singur element in lista
        Ora *sterge = Ora::cap;
        Ora::inainte();
        Ora *before = sterge->prev;
        Ora *after = sterge->next;
        before->addNext(after);
        delete sterge;
    }
    static void goleste() { //goleste intreaga lista
      while(cap!=NULL) sterge();
    }
};
Ora *Ora::cap = NULL;
