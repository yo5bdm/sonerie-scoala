/*  
   Aceasta este partea software a unei instalatii automate de sonerie pentru scoli
    Pentru mai multe detalii, cititi fisierul readme.md
    Adresa proiectului https://gitlab.com/yo5bdm/sonerie-scoala
    Copyright (C) 2018  Erdei Rudolf (https://erdeirudolf.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class Memorie {
  //structura datelor din memorie
private:
  const static byte startMem = 0xAF;
  //enabled;
  //zile sunat
  //structura orei
  //ora;
  //minut;
  //final structura
  const static byte endMem = 0xFA;

  static bool memOK;
  static int bytePtr; //byte pointer - pozitia in memorie


public:
  static bool incarca() {
    if(!Memorie::eepromOk()) return false;
    Memorie::bytePtr = 1;
    //enabled?
    if(EEPROM.read(Memorie::bytePtr)==1) enabled = true;
    else enabled = false;
    Memorie::bytePtr++;
    //zile sunat
    for(int i=0;i<7;i++) {
      ziua[i] = EEPROM.read(Memorie::bytePtr); Memorie::bytePtr++;
    }
    //ore sunat
    byte h, m; //byte citire
    while(EEPROM.read(Memorie::bytePtr)!=Memorie::endMem) {
      h = EEPROM.read(Memorie::bytePtr); Memorie::bytePtr++;
      m = EEPROM.read(Memorie::bytePtr); Memorie::bytePtr++;
      Ora::insereaza(h,m);
    }
    return true;
  }
  
  static int salveaza() {
    pr.mesaj("Se salveaza...");
    //Memorie::eraseEEPROM();
    Memorie::bytePtr = 0;
    EEPROM.write(Memorie::bytePtr,Memorie::startMem); Memorie::bytePtr++; //inceputul de memorie
    //acum e pornita soneria?
    if(enabled) {
      EEPROM.write(Memorie::bytePtr,1); Memorie::bytePtr++;
    }
    else {
      EEPROM.write(Memorie::bytePtr,0); Memorie::bytePtr++;
    }
    //zilele de sunat
    for(int i=0;i<7;i++) {
      EEPROM.write(Memorie::bytePtr,ziua[i]); Memorie::bytePtr++;
    }
    //orele de sunat
    if(Ora::cap != NULL) {
      Ora *ptr = Ora::cap;
      do {
        EEPROM.write(Memorie::bytePtr,ptr->ora); Memorie::bytePtr++;
        EEPROM.write(Memorie::bytePtr,ptr->minut); Memorie::bytePtr++;  
        ptr = ptr->next;
      } while(ptr != Ora::cap);
    }
    EEPROM.write(Memorie::bytePtr,Memorie::endMem);
    pr.mesaj(" gata");
  }

private:
  static void eraseEEPROM() {
    for (int i = 0 ; i < EEPROM.length() ; i++) {
      EEPROM.write(i, 0);
    }
  }

  static bool eepromOk() {
    if(EEPROM.read(0)!=Memorie::startMem) {
      Memorie::memOK = false;
      return false;
    }
    for(int i=1;i<EEPROM.length();i++) {
      if(EEPROM.read(i)==Memorie::endMem) {
        Memorie::memOK = true;
        return true;
      }
    }
    Memorie::memOK = false;
    return false;
  }

};
bool Memorie::memOK=false;
int Memorie::bytePtr=0;

  //https://www.arduino.cc/en/Reference/EEPROM
