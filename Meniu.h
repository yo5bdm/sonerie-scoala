/*  
    Aceasta este partea software a unei instalatii automate de sonerie pentru scoli
    Pentru mai multe detalii, cititi fisierul readme.md
    Adresa proiectului https://gitlab.com/yo5bdm/sonerie-scoala
    Copyright (C) 2018  Erdei Rudolf (https://erdeirudolf.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

void actiune(int bt);
int butonOK();
int butonApasat();
void printMenu();
void meniu(int btn);
void actiune(int bt);
void activeazaSoneria(int btn);
void adaugaOraNoua(int btn);
void stergeOra(int btn);
void seteazaZile(int btn);
void seteazaOra(int btn);
//------------------------------
int butonOK() {
  int ok = digitalRead(butoane[3]);
  if(ok==LOW) { 
    delay(debounce);
    while(digitalRead(butoane[3])==LOW);
    return 1; 
  }
  else { return 0; }
}

int butonApasat() {
  int bP = -1, rBp = -1;
  while(bP == -1) {
    for (int t = 0; t<nrButoane;t++) {
      if (digitalRead(butoane[t]) == LOW) { bP = t; }
    }
  }
  rBp = bP;
  delay(debounce);
  while (bP != 0) { // wait while the button is still down
    bP = 0;
    for(int t = 0; t<nrButoane;t++) {
      if (digitalRead(butoane[t]) == LOW) {bP = t;}
    }
  }
  return rBp;
}

void printMenu() {
top:
  pr.sterge();
  if(nrMeniu<0 || nrMeniu > 4) nrMeniu = 0;
  if(nrSubmeniu<0) {
    pr.mesajSus("Setari:");
    pr.mesajJos(menu[nrMeniu]);
  } else {    
    switch(nrMeniu) {
      case 0:
        pr.mesajSus(submeniu[nrSubmeniu]); //enabled
        if(enabled==true) {
          pr.mesajJos("Activat");
        } else {
          pr.mesajJos("Dezactivat");
        }
        break;
      case 1: //adauga ora
        pr.mesajSus(submeniu[nrSubmeniu+1]);
        pr.mesajJos(Ora::afOra(oraNoua));
        break;
      case 2: //sterge ora
        pr.mesajSus(submeniu[nrSubmeniu+3]);
        pr.mesajJos(Ora::afOra(Ora::cap));
        break;
      case 3: //seteaza zile
        pr.mesajSus(submeniu[nrSubmeniu+4]);
        //String str="";
        //if(ziua[ziuaDeSetat]==1) str ="DA";
        //else str = "NU";
        pr.mesajJos(zileText[ziuaDeSetat]+" "+((ziua[ziuaDeSetat]==1)?"DA":"NU"));
        break;
      case 4://seteaza ora
        pr.mesajSus(submeniu[nrSubmeniu+5]);
        if(nrSubmeniu==1) {
          pr.mesajJos(months[setOra[nrSubmeniu]]);
        } else {
          pr.mesajJos(String(setOra[nrSubmeniu]));
        }
        break;
      default:
      break;
    }
  }
}

void meniu(int btn) {
  switch(btn) {
    case 0: //cancel
      if(nrSubmeniu==-1) {
        Ora::rew();
        lcd.setBacklight(HIGH); //stingem lumina
        stat = 0;
      }
      else actiune(btn); 
      break;
    case 1: //up
      if(nrSubmeniu == -1) nrMeniu = (nrMeniu-1)%dimMeniu;
      else actiune(btn);
      break;
    case 2: //down
      if(nrSubmeniu == -1) nrMeniu = (nrMeniu+1)%dimMeniu;
      else actiune(btn); 
      break;
    case 3: //ok
      if(nrSubmeniu == -1) nrSubmeniu++;
      else actiune(btn);
      break;
    default:
      break;
  }
  if(nrMeniu>=dimMeniu) nrMeniu = 0;
}

void actiune(int bt) { //suntem deja intr-un submeniu, functia specifica a fiecarui submeniu
  switch(nrMeniu) {
    case 0:
      activeazaSoneria(bt); break;
    case 1:
      adaugaOraNoua(bt); break;
    case 2:
      stergeOra(bt); break;
    case 3:
      seteazaZile(bt); break; 
    case 4:
      seteazaOra(bt); break;
    default:
    break;
  }
}

void activeazaSoneria(int btn) {
  if(btn==1 || btn==2) enabled = !enabled;
  if(btn==0 || btn==3) {
    Ora::rew();
    Memorie::salveaza();
    nrSubmeniu = -1;
  }
}

void adaugaOraNoua(int btn) {
  if(btn==1 || btn==2) {
    switch(nrSubmeniu) {
      case 0://ora
        if(btn==1) oraNoua->ora = (oraNoua->ora+1)%24; else oraNoua->ora = (oraNoua->ora-1)%24;
        break;
      case 1://minut
        if(btn==1) oraNoua->minut = (oraNoua->minut+5)%60; else oraNoua->minut = (oraNoua->minut-5)%60;
        break;
    }
  }
  if(oraNoua->ora > 23) oraNoua->ora = 0;
  if(oraNoua->minut >59) oraNoua->minut = 0;
  if(btn == 3) { //ok
    if(nrSubmeniu<1) {
      nrSubmeniu++;
    } else {
      Ora::insereaza(oraNoua->ora, oraNoua->minut); //inseram in lista
      oraNoua->ora = 8; oraNoua->minut = 0; //reset ora noua
      Memorie::salveaza(); //salvam si pe eeprom
      nrSubmeniu = -1; //exit
    }
  }
  if(btn == 0) { //cancel
    nrSubmeniu = -1;
  }
}

void stergeOra(int btn) {
  switch(btn) {
    case 0: //cancel
      Ora::rew();
      nrSubmeniu = -1;
      break;
    case 1: //inapoi
      Ora::inapoi();
      break;
    case 2: //inainte
      Ora::inainte();
      break;
    case 3: //ok
      Ora::sterge();
      Memorie::salveaza();
      Ora::rew();
      break;
    default:
    break;
  }
}

void seteazaZile(int btn) {
  //ziuaDeSetat - numarul zilei de setat curente
  switch(btn) {
    case 0: //cancel / back
      Memorie::salveaza();
      nrSubmeniu = -1;
      break;
    case 1: //inapoi
    case 2: //inainte
      if(ziua[ziuaDeSetat] == 0) ziua[ziuaDeSetat] = 1;
      else ziua[ziuaDeSetat] = 0;
      break;
    case 3: //ok
      ziuaDeSetat = (ziuaDeSetat+1)%7;
      break;
    default:
    break;
  }
}

void seteazaOra(int btn) {
  //int setOra[5] = {2018, 3, 13, 8, 0}; //an, luna, zi, ora, minut
  if(btn==1) {
    setOra[nrSubmeniu]++;
  }
  if(btn==2) {
    setOra[nrSubmeniu]--;
  }
  
  if(setOra[1] > 12) setOra[1] = 0; //luna
  if(setOra[2] > 31) setOra[2] = 0; //ziua
  if(setOra[3] > 23) setOra[3] = 0; //ora
  if(setOra[4] > 59) setOra[4] = 0; //min
  
  if(btn == 3) { //ok
    if(nrSubmeniu<4) {
      nrSubmeniu++;
    } else {
      //setam ora pe RTC
      // sample input: date = "Dec 26 2009", time = "12:34:56"
      String yr = months[setOra[1]]+" "+String(setOra[2])+" "+String(setOra[0]);
      char yrc[12]; yr.toCharArray(yrc,12);
      String hr = ((setOra[3]<10)?"0"+String(setOra[3]):String(setOra[3]))+":"+((setOra[4]<10)?"0"+String(setOra[4]):String(setOra[4]))+":00";
      char hrc[9]; hr.toCharArray(hrc,9);
      DateTime unu = DateTime(yrc,hrc);
      Serial.println(String(yrc)+" "+String(hrc));
      RTC.adjust(unu);
      Serial.println("RTC Adjusted");
      nrSubmeniu = -1; //exit
    }
  }
  if(btn == 0) { //cancel
    nrSubmeniu = -1;
  }
}


