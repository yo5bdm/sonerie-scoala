/*  
    Aceasta este partea software a unei instalatii automate de sonerie pentru scoli
    Pentru mai multe detalii, cititi fisierul readme.md
    Adresa proiectului https://gitlab.com/yo5bdm/sonerie-scoala
    Copyright (C) 2018  Erdei Rudolf (https://erdeirudolf.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define I2C_ADDR 0x3F // adresa afisajului
#define BACKLIGHT_PIN 3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7
LiquidCrystal_I2C  lcd(I2C_ADDR, En_pin, Rw_pin, Rs_pin, D4_pin, D5_pin, D6_pin, D7_pin);


class PrintLCD {
private:
    bool mainScr;
    bool schOraSunat;
public:
  void init() {
    lcd.begin(16, 2);              // initialize the lcd
    lcd.setBacklight(HIGH);
    lcd.home();                   // go home
    lcd.setCursor ( 0, 0 );        // go to the next line
    mainScr = false;
    schOraSunat = true;
  }
  void eroare(String msg) {
    mainScr = false;
    this->mesaj(msg);
  }

  void mainScreen() {
    if(mainScr == true) return;
    lcd.clear();
    lcd.setCursor(0, 1);
    if(enabled && ziua[ziuaAcum]==1) {
      lcd.print("Suna:");
    } else if(ziua[ziuaAcum]==0) {
      lcd.print("Zi libera");
    } else {
      lcd.print("Dezactivat");
    }
    mainScr = true;
  }
  
  void sterge() {
    lcd.clear();
    mainScr = false;
  }
  void mesaj(String msg) {
    lcd.clear();
    lcd.setCursor ( 0, 0 );        // go to the next line
    lcd.print(msg);
    mainScr = false;
  }
  void suna() {
    schOraSunat = true;
  }
  void oraSunat() {
    if(!schOraSunat) return;
    lcd.setCursor(6, 1);
    lcd.print(Ora::afOra(Ora::cap));  
  }
  void oraCurentaPrint() {
    lcd.setCursor(0, 0);
    lcd.print(Ora::afOra(oraCurenta));
    lcd.print(" "+zileText[ziuaAcum]);  
  }
  void mesajSus(String msg) {
    lcd.setCursor(0, 0);
    lcd.print(msg);
    mainScr = false;
  }
  void mesajJos(String msg) {
    lcd.setCursor(0, 1);
    lcd.print(msg);
    mainScr = false;
  }
};

PrintLCD pr;
